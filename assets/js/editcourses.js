let adminUser = localStorage.getItem("isAdmin");
let cardFooter;

//fetch the courses from our API
fetch('https://lamji-capstone2.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {
    // console.log(data)
    let courseData;

    // if the number courses fetch is less than 1, display no courses available
    if(data.length < 1){
        courseData = "No courses available"
    }else{
    // else iterate the courses collection and display
        courseData = data.map(course => {
            return (
                    `  <nav class="navbar navbar-expand-lg navbar-light bg-secondary col-12 mt-1 p-0">
    <a class="navbar-brand text-white col-6 p-0 px-3" href="#">${course.name}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#coursenav" aria-controls="coursenav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="coursenav">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown p-0">
          <a class="nav-link dropdown-toggle text-white p-0 mx-2" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Description
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item disabled" href="#">${course.description}</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white p-0" href="#">Price : ${course.price}</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <button type="button" class="btn btn-sm btn-danger m-2">Delete</button>
                      <a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary mx-2 text-white btn-sm editButton">Update</a></td>
      </form>
    </div>
  </nav>`
                )
        }).join("") //since the collection is an array, use the join() to indicate the separator of each element. we replaced the comma with empty string
    }

    let container = document.querySelector('#coursesContainer')
    container.innerHTML = courseData
})
