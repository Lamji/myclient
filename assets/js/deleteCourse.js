let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId")
let token = localStorage.getItem("token");
let deleteButton = document.querySelector("#delete")
let courseName = document.querySelector("#courseName");

deleteButton.addEventListener("click", (e) => {
	e.preventDefault()

	fetch(`https://lamji-capstone2.herokuapp.com/api/courses/${courseId}`, {
		method: "DELETE",
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
	})
	.then(res => {
		return res.json()
	})
	.then((data) => {
		//creation of new course successful
		if(data === true){
			//redirect to courses index page
			window.location.replace("./courses.html")
		}else{
			//error in creating course, redirect to error page
			alert("Something went wrong")
		}
	})
})
fetch(`https://lamji-capstone2.herokuapp.com/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {
	courseName.innerHTML = data.name
})


