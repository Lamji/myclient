let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let profile = document.querySelector("#profile")
let courses = document.querySelector("#courses")
let student = document.querySelector("#student")
let cardFooter;

if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML = `<div class="p-0 px-1">
	<a href="./addCourse.html" class="nav-link text-dark">Add Course</a>
</div>`
}

//fetch the courses from our API
fetch('https://lamji-capstone2.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {
	// console.log(data)
	let courseData;

	//if the number of courses fetched is less than 1, display no courses available.
	if(data.length < 1){
		courseData = "No courses available."
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
			// console.log(course._id)
			if(adminUser == "false" || !adminUser){
				//check if the user is not an admin
				//if not an admin, display the select course button
				cardFooter = `<a href="./login.html"  class="btn btn-primary m-1 btn-sm text-white btn-block editButton">Select Course</a>`
			}else{
				//if user is an admin, display the edit course button
				cardFooter = `<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton">Edit</a>

<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton">Delete</a>`
			}

			return (
				`
				<div class="col-3 mt-2" >
					<div class="card p-2 px-3 py-3 shadow bg-secondary text-white" id="card-course">
						<h4 class="text-center pt-3">${course.name}</h4>
						<div class="card-body bg-light text-muted shadow">
						${course.description}
						</div>
						<p class=" text-right px-3">₱: ${course.price}</p>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
							Get Course
						  </button>
					</div>
				</div>
			`
					)

		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. We replaced the commas with an empty strings to remove them.
	}

	let container = document.querySelector('#coursesContainer')
	container.innerHTML = courseData
})