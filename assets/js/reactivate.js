let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let title = document.querySelector("#title")
let profile = document.querySelector("#profile")
let cardFooter;


//fetch the courses from our API
fetch('https://lamji-capstone2.herokuapp.com/api/courses/trash')
.then(res => res.json())
.then(data => {
	console.log(data)
	// console.log(data)
	let courseData;

	//if the number of courses fetched is less than 1, display no courses available.
	if(data.length < 1){
		courseData = `   <div class="container text-center p-5">
        <h6>No courses available.</h6>
    </div>`
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
			// console.log(course._id)
			if(adminUser == "false" || !adminUser){
				//check if the user is not an admin
				//if not an admin, display the select course button
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton">Select Course</a>`
			}else{
				//if user is an admin, display the edit course button
				cardFooter = `<a href="./reactivatecourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary px-3 text-white m-1 btn-sm editButton">Reactivate</a>`
			}

			return (
					`	<div class="col-3 px-1 mx-2" >
					<div class="card p-2 px-3 py-3 shadow bg-secondary text-white" id="card-course">
						<h4 class="text-center p-1">${course.name}</h4>
						${cardFooter}
					</div>
				</div>`
					)

		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. We replaced the commas with an empty strings to remove them.
	}

	let container = document.querySelector('#coursesContainer')
	container.innerHTML = courseData
})