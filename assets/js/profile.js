let adminUser = localStorage.getItem("isAdmin")
//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local storage
let userId = localStorage.getItem("id");
let profileContainer = document.querySelector("#profileContainer")
let courseContainer = document.querySelector("#courseContainer")



//fetch the courses from our API
fetch('https://lamji-capstone2.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
    console.log(data.email)
   profileContainer.innerHTML = `
       <div class="card-body">
       <img src="../assets/images/avatar.png" class="user-img" alt="">
       <hr>
           <h5 class="card-title mb-0 bg-secondary text-white p-2">${data.firstName} ${data.lastName}</h5>
           <p class="card-title text-left m-0 p-0 px-2" id="courseList"><img src="../assets/images/phone.png" class="phone-img " alt="">${data.mobileNo}</p>
           <p class="card-title text-left m-0 p-0 px-2" id="courseList">Email: ${data.email}</p>
       </div>
  `
    let courseIds = []
    let courseNames = []
    let courseDes = []
        let courseEnrolled = data.enrollments.map(course => {
            courseIds.push(course.courseId)
           
        })
       
        
        fetch(`https://lamji-capstone2.herokuapp.com/api/courses`)
        .then(res => res.json())
        .then(data => {
            for(let i = 0; i < data.length; i++){
                 console.log(courseNames)
                if(courseIds.includes(data[i]._id)){
                    courseNames.push(data[i].name)
                    courseDes.push(data[i].description)	
                }
            }
            let coursesList = courseNames.join("<br>")
            let desList = courseDes.join("<br>")
            courseContainer.innerHTML = `<div class="row m-0">
            <div class="col-12 text-left  p-2 bg-light">
            <p class="p-2 text-left mb-0 bg-secondary text-white">Course Name:</p>
            <p class="px-3" id="courseList">${coursesList}</p> 
            </div>
        </div>`
            
        })

})

