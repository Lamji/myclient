let adminUser = localStorage.getItem("isAdmin")
//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local storage
let userId = localStorage.getItem("id");
let profileContainer = document.querySelector("#profileContainer")
let courseContainer = document.querySelector("#courseContainer")
let userContainer = document.querySelector("#userContainer")


console.log(userId)
fetch('https://lamji-capstone2.herokuapp.com/api/users')
.then(res => res.json())
.then( data => {
    console.log(data)
   for(let i = 0; i < data.length; i++){
       let fullName = data[i].lastName + ", " + data[i].firstName
       let firstName = data[i].firstName
       let mobileNo = data[i].mobileNo
        if(firstName === "admin"){
            userContainer.innerHTML =""
        }
        let courseIds = []
        let courseName = []
        let Enrolled = data[i].enrollments
        for(let i = 0; i < Enrolled.length; i++){
            courseIds.push(Enrolled[i].courseId)
          
        }
        // let coursesList = courseIds.join("<br>")
        fetch(`https://lamji-capstone2.herokuapp.com/api/courses`)
        .then(res => res.json())
        .then(data => {
            for(let i = 0; i < data.length; i++){
                if(courseIds.includes(data[i]._id)){
                    courseName.push(data[i].name)
                   
                }
            }
            let coursesList = courseName.join("<br>")
            userContainer.innerHTML += `
            <div class="card-body py-1">
                <h5 class="card-title mb-0 bg-secondary text-white p-2">${fullName}</h5>
                <p class="card-title text-left m-0 p-0 px-2" id="courseList"><img src="../assets/images/phone.png" class="phone-img " alt="">${mobileNo}</p>
                <p class="card-title text-left m-0 p-0 px-2" id="courseList">Course Enrolled: <br>${coursesList}</p>
            </div>
        `
            })
            
        }
       
   })

