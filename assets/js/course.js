//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local storage
let userId = localStorage.getItem("id");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`https://lamji-capstone2.herokuapp.com/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {
	courseName.innerHTML = `<div class="card p-2 text-center">${data.name}</div>`
	courseDesc.innerHTML = `<div class="card p-2" id="description"><p class="mb-0 bg-secondary p-2 text-center text-white">Course Description:</p><p class="p-2 mb-0">${data.description}</p></div>`
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary my-2">Enroll</button>`
	let enrollButton = document.querySelector("#enrollButton")
	enrollButton.addEventListener("click", () => {
		//enroll the user for the course
		let enrollCourseId = data._id
		console.log(enrollCourseId)
		//fetch the courses from our API
		fetch('https://lamji-capstone2.herokuapp.com/api/users/details', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			let courseEnrolled = data.enrollments.map(course => {
				// courseIds.push(course.courseId)	
				if(enrollCourseId.includes(course.courseId)){
					
				} else{
					
				}
				
			})
			
			
		})
		fetch('https://lamji-capstone2.herokuapp.com/api/users/enroll', {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: userId
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
		
			if(data === true){
				//enrollment is successful
				alert("Thank you for enrolling! See you!")
				window.location.replace('./courses.html')
			}else{
				alert("Enrollment failed")
			}
		})
	})
})